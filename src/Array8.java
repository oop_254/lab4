import java.util.Scanner;

public class Array8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int arr[] = new int[3];
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            System.out.print("Please input arr[" + i + "]: ");
            arr[i] = sc.nextInt();
            sum = arr[i] + sum;
        }
        System.out.print("arr = ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
        System.out.println("sum = " + sum);

        double avg = ((double)sum) / arr.length;
        System.out.println("avg = " + avg);

        int index = 0;
        for(int i=1; i<arr.length; i++) {
            if(arr[index] < arr[1]) {
                index = i;
            }
        }
        System.out.println("min = " + arr[index]);

        int index2 = 0;
        for(int i=1; i<arr.length; i++) {
            if(arr[i] > arr[index2]) {
                index2 = i;
            }
        }
        System.out.println("max = " + arr[index2]);
    }
}
